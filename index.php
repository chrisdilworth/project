<?php
include 'header.html';
?>

<?php

$items_per_pg = $_COOKIE['item'];

if(isset($_GET['page']) && !empty($_GET['page'])){
  $currentPage = $_GET['page'];
}else{
  $currentPage = 1;
}

$offset = ($currentPage * $items_per_pg) - $items_per_pg;

// open database

$names = array();
$prices = array();
$images = array();

class MyDB extends SQLite3 {
  function __construct() {
      $this->open('project.db');
  }
}

$db = new MyDB();
if(!$db) {
  echo $db->lastErrorMsg();
}

// Find out how many num_products exist in db ($num_products)
$sql = " select count(name) from products ";
$ret = $db->query($sql);
if(!$ret){
  echo $db->lastErrorMsg();
}
$row = $ret->fetcharray();
$num_products = $row[0];

$lastPage = ceil($num_products/$items_per_pg);
$firstPage = 1;
$nextPage = $currentPage + 1;
$previousPage = $currentPage - 1;

$sql = " select name, price, image from products limit $offset, $items_per_pg ";
$ret = $db->query($sql);
if(!$ret){
  echo $db->lastErrorMsg();
}

$index = 1;
while($row = $ret->fetcharray()){
  $names[$index] = $row[0];
  $prices[$index] = $row[1];
  $images[$index] = $row[2];
  $index++;
}
?>
<div class='row-spacer'></div><div class='row5'><div class='col100 header'><h3>
<?php if($currentPage != $firstPage) { ?>
<a href="?page=<?php echo $firstPage ?>" tabindex="-1">First</a>
<?php } ?>
<?php if($currentPage >= 2) { ?>
<a href="?page=<?php echo $previousPage ?>"><?php echo $previousPage ?></a>
<?php } ?>
<a href="?page=<?php echo $currentPage ?>"><?php echo $currentPage ?></a>
<?php if($currentPage != $lastPage) { ?>
<a href="?page=<?php echo $nextPage ?>"><?php echo $nextPage ?></a>
<a href="?page=<?php echo $lastPage ?>">Last</a>
<?php } ?>
</h3></div></div>

<?php
$totalit = sizeOf($names);
$itemnum = 1;

while($totalit > 0){
  echo "<div class='product'><a href='product-".$itemnum.".php'><img src='./assets/img/product-".$itemnum.".jpg'></a><p>".$names[$itemnum]."</p><p>$".$prices[$itemnum]."</p></div>";
  $totalit--;
  $itemnum++;
}

?>

<?php
include 'footer.html';
?>