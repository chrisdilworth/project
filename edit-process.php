<?php

// For processing edit-n.php files

$id = $image = $name = $price = $descr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
  $id = test_input($_POST["id"]);
  $image = test_input($_POST["image"]);
  $name = test_input($_POST["name"]);
  $price = test_input($_POST["price"]);
  $descr = test_input($_POST["descr"]);
}

function test_input($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

class MyDB2 extends SQLite3 {
  function __construct() {
      $this->open('project.db');
  }
}

$db2 = new MyDB2();
if(!$db2) {
  echo $db2->lastErrorMsg();
}

$sql = "update products set name = '$name', price = $price, descr = '$descr' where ID = $id";

$db2->exec($sql);
if(!$db2) {
  echo $db2->lastErrorMsg();
}

?>
<?php
  // regenerate all product-n.php and edit-n.php
  include 'update.php';

  // redirect to home page
  header('Location: index.html');
?>