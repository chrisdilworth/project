<?php
  include 'header.html';
?>

    <div class="row-spacer"></div>

    <div class="row5">
      <div class="col100 header">
        <h3>To contact us:</h3>
      </div>
    </div>
    
    <div class="textblock">
      <div class="text-leftj">
        <p>Phone: 123.456.7890</p>
        <p>&nbsp;</p>
        <p>Email: info@company.com</p>
        <p>&nbsp;</p>
        <p>Company name</p>
        <p>Address line 1</p>
        <p>Address line 2</p>
        <p>City, Province,</p>
        <p>Code</p>
        <p>CANADA</p>
      </div>
    </div>

<?php
  include 'footer.html';
?>