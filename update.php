<?php

  // open database

  $ids = array();
  $names = array();
  $prices = array();
  $images = array();
  $descr = array();
  $index = 1;

  class MyDB extends SQLite3 {
    function __construct() {
        $this->open('project.db');
    }
  }

  $db = new MyDB();
  if(!$db) {
    echo $db->lastErrorMsg();
  }

  $sql = "select * from products;";

  $ret = $db->query($sql);

  if(!$ret){
    echo $db->lastErrorMsg();
  }

  while($row = $ret->fetcharray()){
    $ids[$index] = $row[0];
    $names[$index] = $row[1];
    $prices[$index] = $row[2];
    $images[$index] = $row[3];
    $descr[$index] = $row[4];
    $index++;
  }

  $index = count($names);
  $txt1 = "<?php include 'header.html' ?>";
  $txt3 = "<?php include 'footer.html' ?>";
  $txt4 = "";

  while($index > 0){

    // write product-n.php files
    $myfile = fopen("product-".$index.".php", "w");
    fwrite($myfile, $txt1);
    $txt2 = "
      <div class='product'>
        <img src='/assets/img/product-".$index.".jpg'>
      </div>
      <div class='prod-descr'>
        <h3>".$names[$index]."</h3>
        <h3>$".$prices[$index]."</h3>
        <h3>Product Description:</h3>
        <p>".$descr[$index]."</p>
      </div>";
    fwrite($myfile, $txt2);
    fwrite($myfile, $txt3);

    // write edit-n.php files
    $myfile = fopen("edit-".$index.".php", "w");
    fwrite($myfile, $txt1);
    $txt2 = "
      <form action='edit-process.php' target='_same' method='post'>        
      <input class='textb' type='hidden' name='id' value='".$ids[$index]."' />
        <div class='product'>
          <p>Product image:</br>
          <input class='textb' type='text' name='image' value='/assets/img/product-".$index.".jpg' required /></p>
        </div>
        <div class='prod-descr'>
          <p>Product name:</br>
          <input class='textb' type='text' name='name' value=".$names[$index]." required /></p>
          <p>Product price:</br>
          <input class='textb' type='text' name='price' value=".$prices[$index]." required /></p>
          <p>&nbsp;</p>
          <p>Product Description:</p>
          <textarea class='texta' type='text' name='descr'>".$descr[$index]." required></textarea>
        </div>
        <div>
          <p>&nbsp;</p>
          <input type='submit' class='button-search' name='submit' value='Update' />
        </div>
      </form>";
    fwrite($myfile, $txt2);
    fwrite($myfile, $txt3);

    // write drop-down list of product names for search.php & admin.php
    $txt4 = $txt4."<option class='options' value=".$index.">".$names[$index]."</option>";
    $index--;
  }

  // write search.php file
  $myfile = fopen("search.php", "w");
  fwrite($myfile, $txt1);
  // insert drop-down list of product names
  $txt2 = "
    <div class='row-spacer'></div>
    <div class='row5'>
      <div class='col100 header'>
        <h3>Search (Login to edit)</h3>
      </div>
    </div>
    <div class='prod-descr'>
      <form action='search-process.php' target='_same' id='prodlist' method='post'>
        <select class='textb' name='names' form='prodlist'>".$txt4."</select>
        <div>
          <p>&nbsp;</p>
          <button class='button-search' type='submit'>Search</button>
        </div>
      </form>
    </div>";
  fwrite($myfile, $txt2);
  fwrite($myfile, $txt3);

  // write admin.php file
  $myfile = fopen("admin.php", "w");
  fwrite($myfile, $txt1);
  // insert drop-down list of product names
  $txt2 = "
    <?php
    session_start();
    ?>
    <div class='row-spacer'></div>
    <div class='row5'>
      <div class='col100 header'>
        <h3 class='admin'>Welcome! You can now edit:</h3>
      </div>
    </div>
    <div class='prod-descr'>
      <form action='admin-process.php' target='_same' id='prodlist' method='post'>
        <select class='textb' name='names' form='prodlist'>".$txt4."</select>
        <div>
          <p>&nbsp;</p>
          <button class='button-search' type='submit'>Search</button>
        </div>
      </form>
    </div>";
  fwrite($myfile, $txt2);
  fwrite($myfile, $txt3);


  unset($names, $prices, $images, $row, $ret, $db);

?>